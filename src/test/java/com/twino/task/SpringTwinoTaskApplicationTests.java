package com.twino.task;

import com.twino.task.model.LoanApplication;
import com.twino.task.service.LoanApplicationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringTwinoTaskApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testScoring(){
		LoanApplicationService service = new LoanApplicationService(null);

		LoanApplication application = new LoanApplication();
		application.setBirthDate(Instant.parse("1996-02-06T18:01:55.648475Z"));
		application.setFirstName("Abc");
		application.setSalary(99999);
		application.setMonthlyLiability(200);
		service.calculateStatus(application);
		Assert.assertEquals("Approved", application.getStatus());


		LoanApplication application2 = new LoanApplication();
		application2.setBirthDate(Instant.parse("1996-02-06T18:01:55.648475Z"));
		application2.setFirstName("Abc");
		application2.setSalary(300);
		application2.setMonthlyLiability(1000);
		service.calculateStatus(application2);
		Assert.assertEquals("Rejected", application2.getStatus());


		LoanApplication application3 = new LoanApplication();
		application3.setBirthDate(Instant.parse("1996-01-01T18:01:55.648475Z"));
		application3.setFirstName("Abc");
		application3.setSalary(1000);
		application3.setMonthlyLiability(50);
		service.calculateStatus(application3);
		Assert.assertEquals("Manual", application3.getStatus());
	}

}

package com.twino.task.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.lang.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanApplication {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String personalId;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private Instant birthDate;

    @NonNull
    private String employer;

    @NonNull
    private double salary;

    @NonNull
    private double monthlyLiability;

    @NonNull
    private double requestedAmount;

    @NonNull
    private String requestedTerm;

    private String status;

    //* Personal ID
    //* First Name
    //* Last Name
    //* Birth Date
    //* Employer (as a String)
    //* Salary (as decimal amount)
    //* Monthly Liability - sum of all liability payments per month (as decimal amount)
    //* Requested Amount (as decimal amount)
    //* Requested Term (days or months)

}
package com.twino.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class LoanApplicationDto {
    private Long id;
    private String personalId;
    private String firstName;
    private String lastName;
    private Instant birthDate;
    private String employer;
    private double salary;
    private double monthlyLiability;
    private double requestedAmount;
    private String requestedTerm;
    private String status;
}

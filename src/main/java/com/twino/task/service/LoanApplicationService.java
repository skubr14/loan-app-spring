package com.twino.task.service;

import com.twino.task.dto.LoanApplicationDto;
import com.twino.task.exceptions.SpringCustomException;
import com.twino.task.model.LoanApplication;
import com.twino.task.repository.LoanApplicationRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j

public class LoanApplicationService {

    private final LoanApplicationRepository loanApplicationRepository;

    @Transactional
    public LoanApplication save (LoanApplicationDto loanApplicationDto){
        LoanApplication loanApplication = mapLoanApplicationDto(loanApplicationDto);
        calculateStatus(loanApplication);

        loanApplicationRepository.save(loanApplication);
        return loanApplication;
    }

    @Transactional
    public LoanApplication update(long loanApplicationId, String status) {
        LoanApplication application = loanApplicationRepository.findById(loanApplicationId).orElseThrow(() -> new SpringCustomException("Loan not found with name - " + loanApplicationId));
        application.setStatus(status);
        loanApplicationRepository.save(application);
        return application;
    }

    public void calculateStatus(LoanApplication loanApplication){
        int score = 0;
        String firstName = loanApplication.getFirstName();
        for(char c : firstName.toCharArray()){
            int temp = (int)Character.toLowerCase(c);
            int temp_integer = 96;
            if(temp<=122 & temp>=97) {
                score += (temp - temp_integer);
            }
        }
        score += loanApplication.getSalary() * 1.5;
        score -= loanApplication.getMonthlyLiability() * 3;
        ZonedDateTime birthDate = loanApplication.getBirthDate().atZone(ZoneOffset.UTC);
        int birthYear = birthDate.getYear();
        score += birthYear;
        int monthOfBirth = birthDate.getMonthValue();
        score -= monthOfBirth;

        int julianDay = birthDate.getDayOfYear();
        score -= julianDay;

        System.out.println("Score is: " + score);
        if (score < 2500){
            loanApplication.setStatus("Rejected");
        }else if(score > 3500){
            loanApplication.setStatus("Approved");
        }else{
            loanApplication.setStatus("Manual");
        }

    }

    private LoanApplication mapLoanApplicationDto (LoanApplicationDto loanApplicationDto){
        return LoanApplication.builder()
                .id(loanApplicationDto.getId())
                .personalId(loanApplicationDto.getPersonalId())
                .firstName(loanApplicationDto.getFirstName())
                .lastName(loanApplicationDto.getLastName())
                .birthDate(loanApplicationDto.getBirthDate())
                .employer(loanApplicationDto.getEmployer())
                .salary(loanApplicationDto.getSalary())
                .monthlyLiability(loanApplicationDto.getMonthlyLiability())
                .requestedAmount(loanApplicationDto.getRequestedAmount())
                .requestedTerm(loanApplicationDto.getRequestedTerm()).build();
    }
    @Transactional(readOnly = true)
    public List<LoanApplicationDto> getAll() {
        return loanApplicationRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<LoanApplicationDto> getManualApplications() {
        return loanApplicationRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .filter(loanApplication -> loanApplication.getStatus().equals("Manual"))
                .collect(Collectors.toList());
    }

    private LoanApplicationDto mapToDto(LoanApplication loanApplication) {
        return LoanApplicationDto.builder().personalId(loanApplication.getPersonalId())
                .id(loanApplication.getId())
                .firstName(loanApplication.getFirstName())
                .lastName(loanApplication.getLastName())
                .birthDate(loanApplication.getBirthDate())
                .employer(loanApplication.getEmployer())
                .salary(loanApplication.getSalary())
                .monthlyLiability(loanApplication.getMonthlyLiability())
                .requestedAmount(loanApplication.getRequestedAmount())
                .requestedTerm(loanApplication.getRequestedTerm())
                .status(loanApplication.getStatus()).build();
    }


    public void delete(long loanApplicationId) {
        loanApplicationRepository.deleteById(loanApplicationId);

    }


}

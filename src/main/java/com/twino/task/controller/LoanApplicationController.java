package com.twino.task.controller;

import com.twino.task.dto.LoanApplicationDto;
import com.twino.task.model.LoanApplication;
import com.twino.task.service.LoanApplicationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/loan-application")
@AllArgsConstructor
@Slf4j
public class LoanApplicationController {
    private final LoanApplicationService loanApplicationService;
    //**List**, **Create** and **Delete** applications, to **Mark** applications as *Approved*,
    // *Manual* or *Rejected* and to assign credit score to the application.
    @PostMapping
    public ResponseEntity<LoanApplication> createLoanApplication(@RequestBody LoanApplicationDto loanApplicationDto){
        System.out.println(loanApplicationDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(loanApplicationService.save(loanApplicationDto));
    }

    @PostMapping(path = "/edit/{id}")
    public ResponseEntity<LoanApplication> editLoanApplication(
            @PathVariable("id") long loanApplicationId, @RequestBody String status){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(loanApplicationService.update(loanApplicationId, status));
    }

    @GetMapping(path = "/manualApps")
    public ResponseEntity<List<LoanApplicationDto>> getAllManualApplications() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(loanApplicationService.getManualApplications());
    }

    @GetMapping
    public ResponseEntity<List<LoanApplicationDto>> getAllLoanApplications() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(loanApplicationService.getAll());
    }

    @DeleteMapping("/{id}")
    private void deleteBook(@PathVariable("id") long loanApplicationId)
    {
        loanApplicationService.delete(loanApplicationId);
    }

    //Change status of the application

}